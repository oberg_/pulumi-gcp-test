"use strict";
const pulumi = require("@pulumi/pulumi");
const gcp = require("@pulumi/gcp");
const random = require("@pulumi/random");

const folders = require("./pkg/folders");
const projects = require("./pkg/projects");
const buckets = require("./pkg/buckets");
const serviceAccounts = require("./pkg/serviceAccounts");
