"use strict";
const pulumi = require("@pulumi/pulumi");
const gcp = require("@pulumi/gcp");

const config = new pulumi.Config();

const folders = require("./folders");
const random = require("../utils/random");
const enableApis = require("../modules/enableApis");
const billingAccount = require("../modules/billingAccount")(
  config.require("billingAccount")
);

const provisionerProject = new gcp.organizations.Project("provisioner", {
  name: "provisioner",
  folderId: folders.automationFolderID,
  projectId: pulumi.interpolate`provisioner-${random.randNr("provisionerID")}`,
  billingAccount: billingAccount.id
});

const activateApis = [
  "bigquery-json.googleapis.com",
  "cloudbuild.googleapis.com",
  "containerregistry.googleapis.com",
  "cloudresourcemanager.googleapis.com",
  "admin.googleapis.com",
  "cloudbilling.googleapis.com",
  "serviceusage.googleapis.com",
  "iam.googleapis.com",
  "compute.googleapis.com",
  "servicemanagement.googleapis.com",
  "cloudfunctions.googleapis.com"
];

let services = enableApis(
  "provisioner",
  provisionerProject.id,
  true,
  activateApis
);

exports.provisionerProjectID = provisionerProject.id;
