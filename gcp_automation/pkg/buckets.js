"use strict";
const gcp = require("@pulumi/gcp");

const projects = require("./projects");

const stateBucket = new gcp.storage.Bucket(
  "state-bucket",
  {
    location: "EU",
    project: projects.provisionerProjectID,
    versioning: { enabled: true }
  },
  { customTimeouts: { create: "3m" } }
);

exports.stateBucketID = stateBucket.id;
exports.stateBucketName = stateBucket.name;
