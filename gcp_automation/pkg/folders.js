"use strict";
const pulumi = require("@pulumi/pulumi");
const gcp = require("@pulumi/gcp");

const config = new pulumi.Config();

const coreFolder = new gcp.organizations.Folder("Control", {
  displayName: "Control",
  parent: `organizations/${config.require("orgID")}`
});

const automationFolder = new gcp.organizations.Folder("Automation", {
  displayName: "Automation",
  parent: coreFolder.id
});

exports.coreFolderID = coreFolder.id;
exports.automationFolderID = automationFolder.id;
