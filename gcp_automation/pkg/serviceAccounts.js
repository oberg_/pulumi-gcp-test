"use strict";
const pulumi = require("@pulumi/pulumi");
const gcp = require("@pulumi/gcp");

const config = new pulumi.Config();

const projects = require("./projects");

const provisionerSA = new gcp.serviceAccount.Account("provisioner-org-sa", {
  accountId: "provisioner-org-sa",
  displayName: "Provisioner Organization ServiceAccount",
  project: projects.provisionerProjectID
});

const provisionerSaRoles = [
  "roles/resourcemanager.projectCreator",
  "roles/resourcemanager.folderCreator",
  "roles/resourcemanager.organizationAdmin",
  "roles/iam.organizationRoleAdmin",
  "roles/orgpolicy.policyAdmin",
  "roles/iam.serviceAccountAdmin",
  "roles/billing.user",
  "roles/logging.admin",
  "roles/logging.configWriter",
  "roles/compute.xpnAdmin",
  "roles/serviceusage.serviceUsageAdmin",
  "roles/storage.admin",
  "roles/pubsub.admin",
  "roles/bigquery.admin",
  "roles/iam.serviceAccountUser",
  "roles/cloudfunctions.admin",
  "roles/editor"
];

let provIamRoles = provisionerSaRoles.map((role, idx) => {
  return new gcp.organizations.IAMMember(`prov-org-sa-${role}`, {
    member: pulumi.interpolate`serviceAccount:${provisionerSA.email}`,
    orgId: config.require("orgID"),
    role: role
  });
});

exports.provisionerSA = provisionerSA;
