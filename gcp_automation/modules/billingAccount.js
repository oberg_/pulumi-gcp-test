"use strict";
const gcp = require("@pulumi/gcp");

module.exports = billingAccount => {
  return new gcp.organizations.getBillingAccount({
    billingAccount: billingAccount,
    open: true
  });
};
