"use strict";
const gcp = require("@pulumi/gcp");

const billingAccount = gcp.organizations.getBillingAccount({
  displayName: "My Billing Account",
  open: true
});

exports.id = billingAccount.id;
