"use strict";
const gcp = require("@pulumi/gcp");

module.exports = (bucketName, projectId) => {
  return new gcp.storage.Bucket(
    bucketName,
    {
      location: "EU",
      project: projectId,
      storageClass: "COLDLINE",
      versioning: { enabled: false },
      retentionPolicy: {
        retentionPeriod: "157593600"
      },
      lifecycleRules: [
        {
          action: {
            type: "Delete"
          },
          condition: {
            matchesStorageClasses: ["COLDLINE"],
            withState: "LIVE",
            age: "1826"
          }
        }
      ]
    },
    { customTimeouts: { create: "2m" } }
  );
};
