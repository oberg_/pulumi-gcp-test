"use strict";

const auditProjects = require("./projects");

const auditBucket = require("./auditBucket");

const auditLogBucket = auditBucket("log-bucket", auditProjects.auditProjectID);
const auditBillingBucket = auditBucket(
  "billing-bucket",
  auditProjects.auditProjectID
);

exports.auditLogBucketName = auditLogBucket.name;
exports.auditBillingBucketName = auditLogBucket.name;
