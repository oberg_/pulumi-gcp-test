"use strict";
const pulumi = require("@pulumi/pulumi");
const gcp = require("@pulumi/gcp");

const config = new pulumi.Config();

const auditFolder = new gcp.organizations.Folder("Audit", {
  displayName: "Audit",
  parent: `folders/${config.require("controlFolder")}`
});

exports.auditFolderID = auditFolder.id;
