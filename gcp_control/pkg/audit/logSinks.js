"use strict";
const pulumi = require("@pulumi/pulumi");
const gcp = require("@pulumi/gcp");

const config = new pulumi.Config();

const billingAccount = require("../../modules/billingAccount")(
  config.require("billingAccount")
);

const auditBuckets = require("./buckets");

const billingSink = new gcp.logging.BillingAccountSink(
  pulumi.interpolate`billing-gs-${billingAccount.id}`,
  {
    name: pulumi.interpolate`billing-gs-${billingAccount.id}`,
    billingAccount: billingAccount.id,

    destination: pulumi.interpolate`storage.googleapis.com/${buckets.auditBillingBucketName}`
  }
);

const logSink = new gcp.logging.OrganizationSink("audit-log-gs", {
  name: "audit-log-gs",
  orgId: config.require("orgID"),

  destination: pulumi.interpolate`storage.googleapis.com/${bucket.auditLogBucketName}`,
  includeChildren: true,

  filter:
    'protoPayload.@type = "type.googleapis.com/google.cloud.audit.AuditLog"'
});
