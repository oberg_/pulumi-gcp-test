"use strict";
const pulumi = require("@pulumi/pulumi");
const gcp = require("@pulumi/gcp");

const config = new pulumi.Config();

const billingAccount = require("../../modules/billingAccount")(
  config.require("billingAccount")
);

const auditFolders = require("./folders");
const random = require("../../utils/random");

const auditProject = new gcp.organizations.Project("audit", {
  name: "audit",
  folderId: auditFolders.auditFolderID,
  projectId: pulumi.interpolate`audit-${random.randNr("auditID")}`,
  billingAccount: billingAccount.id
});

exports.auditProjectID = auditProject.id;
