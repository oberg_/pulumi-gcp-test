"use strict";

const folders = require("./folders");
const projects = require("./projects");
const buckets = require("./buckets");

module.exports = {
  folders,
  projects,
  buckets
};
