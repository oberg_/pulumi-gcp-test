"use strict";
const pulumi = require("@pulumi/pulumi");
const gcp = require("@pulumi/gcp");

const securityProjects = require("./projects");
const securityPubsubs = require("./pubsubs");

// Service account to be used by cloud function
const securityCloudFunctionSA = new gcp.serviceAccount.Account(
  "audit-cloudfunction-sa",
  {
    accountId: "audit-cloudfunction-sa",
    displayName: "Audit logging cloudfunction SA",
    project: securityProjects.securityProjectID
  }
);

// IAM for Cloudfunction SA
const securityCloudFunctionSAIAM = new gcp.projects.IAMMember(
  `audit-cloudfunction-sa-logging.admin`,
  {
    member: pulumi.interpolate`serviceAccount:${securityCloudFunctionSA.email}`,
    project: securityProjects.securityProjectID,
    role: "roles/logging.admin"
  }
);

// Storage bucket to host cloudfunction archive
const securityCloudFunctionsBucket = new gcp.storage.Bucket(
  "security-cloudfunctions-bucket",
  {
    project: securityProjects.securityProjectID,
    storageClass: "COLDLINE"
  }
);

// Upload cloudfunction archive
const cloudFunctionArchive = new gcp.storage.BucketObject("auditFunctionZip", {
  bucket: securityCloudFunctionsBucket.name,
  source: new pulumi.asset.FileAsset("./audit_log_cloudfunction.zip")
});

// Create Cloud function
const securityAuditLogCF = new gcp.cloudfunctions.Function(
  "security-auditlog-cloudfunction",
  {
    project: securityProjects.securityProjectID,
    availableMemoryMb: 128,
    description: "Function to tagg auditlogs",
    region: "europe-west1",
    entryPoint: "socAuditLogTransformation",
    runtime: "python37",
    sourceArchiveBucket: securityCloudFunctionsBucket.name,
    sourceArchiveObject: cloudFunctionArchive.name,
    eventTrigger: {
      eventType: "google.pubsub.topic.publish",
      resource: securityPubsubs.securityAuditPubSubTopicID
    },
    serviceAccountEmail: securityCloudFunctionSA.email
  },
  { dependsOn: [securityProjects.securityProjectService] }
);
