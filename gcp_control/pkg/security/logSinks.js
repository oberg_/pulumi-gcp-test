"use strict";
const pulumi = require("@pulumi/pulumi");
const gcp = require("@pulumi/gcp");

const config = new pulumi.Config();

const securityProjects = require("./projects");
const securityPubsubs = require("./pubsubs");

const globalLogSink = new gcp.logging.OrganizationSink("audit-log-ps", {
  name: "audit-log-ps",
  orgId: config.require("orgID"),

  destination: pulumi.interpolate`pubsub.googleapis.com/projects/${securityProjects.securityProjectID}/topics/${securityPubsubs.securityAuditPubSubTopicName}`,
  includeChildren: true,

  filter:
    'protoPayload.@type = "type.googleapis.com/google.cloud.audit.AuditLog"'
});

exports.globalLogSinkWriterIdentity = globalLogSink.writerIdentity;
