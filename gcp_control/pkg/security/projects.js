"use strict";
const pulumi = require("@pulumi/pulumi");
const gcp = require("@pulumi/gcp");

const config = new pulumi.Config();

const billingAccount = require("../../modules/billingAccount")(
  config.require("billingAccount")
);
const securityFolders = require("./folders");

const random = require("../../utils/random");

const securityProject = new gcp.organizations.Project("security", {
  name: "security",
  folderId: securityFolders.securityFolderID,
  projectId: pulumi.interpolate`security-${random.randNr("securityID")}`,
  billingAccount: billingAccount.id
});

const securityProjectService = new gcp.projects.Service(
  `security-project-service`,

  {
    disableDependentServices: true,
    project: securityProject.id,
    service: "cloudfunctions.googleapis.com"
  }
);

exports.securityProjectID = securityProject.id;
exports.securityProjectService = securityProjectService;
