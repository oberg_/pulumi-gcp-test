"use strict";
const pulumi = require("@pulumi/pulumi");
const gcp = require("@pulumi/gcp");

const securityProjects = require("./projects");
const logSinks = require("./logSinks");

const securityAuditPubSubTopic = new gcp.pubsub.Topic("audit-log-ps", {
  name: "audit-log-ps",
  project: securityProjects.securityProjectID
});

const securityAuditPubSubTopicIAM = new gcp.pubsub.TopicIAMBinding(
  "security-pubsub-role",
  {
    project: securityProjects.securityProjectID,
    topic: securityAuditPubSubTopic.name,
    role: "roles/pubsub.publisher",
    members: [logSinks.globalLogSinkWriterIdentity]
  }
);

exports.securityAuditPubSubTopicID = securityAuditPubSubTopic.id;
exports.securityAuditPubSubTopicName = securityAuditPubSubTopic.name;
