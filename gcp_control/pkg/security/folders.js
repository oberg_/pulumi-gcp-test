"use strict";
const pulumi = require("@pulumi/pulumi");
const gcp = require("@pulumi/gcp");

const config = new pulumi.Config();

const securityFolder = new gcp.organizations.Folder("Security", {
  displayName: "Security",
  parent: `folders/${config.require("controlFolder")}`
});

exports.securityFolderID = securityFolder.id;
