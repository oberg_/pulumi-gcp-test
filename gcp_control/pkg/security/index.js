"use strict";

const folders = require("./folders");
const projects = require("./projects");
const pubsubs = require("./pubsubs");
const logSinks = require("./logSinks");
const cloudFunctions = require("./cloudFunctions");

module.exports = {
  folders,
  projects,
  pubsubs,
  logSinks,
  cloudFunctions
};
