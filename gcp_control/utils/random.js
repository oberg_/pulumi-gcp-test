"use strict";
const random = require("@pulumi/random");

module.exports = {
  randNr: resourceName => {
    return new random.RandomId(resourceName, {
      byteLength: 4
    }).dec;
  }
};
