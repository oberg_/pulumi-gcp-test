"use strict";
const pulumi = require("@pulumi/pulumi");
const gcp = require("@pulumi/gcp");

module.exports = (
  urn,
  project,
  disableDependentServices = false,
  services = []
) =>
  services.map(service => {
    return new gcp.projects.Service(`${urn}-${service}`, {
      disableDependentServices,
      project,
      service
    });
  });
