I've followed this to get Pulumi to work with GCP.
https://www.pulumi.com/docs/intro/cloud-providers/gcp/setup/


When standing in one of the folders

pulumi login --local to save state locally. Will be saved in ~/.pulumi
pulumi login -c gs://bucket-url  to store state in a storage bucket

pulumi stack init


in gcp_automation stack make sure the config has orgID and billingAccount set
in gcp_control stack make sure the config has orgID, billingAccount and controlFolder set
